package ru.mtuci.kiib.controller;

import org.apache.commons.codec.Charsets;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.util.encoders.Hex;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.mtuci.kiib.DB.DBNotes;
import ru.mtuci.kiib.bean.HttpSessionBean;
import ru.mtuci.kiib.domain.Note;
import ru.mtuci.kiib.domain.User;
import ru.mtuci.kiib.repository.NoteRepository;
import ru.mtuci.kiib.security.StribogProvider;
import ru.mtuci.kiib.service.UserService;

import javax.crypto.*;
import javax.crypto.spec.SecretKeySpec;
import javax.validation.Valid;
import java.io.IOException;
import java.nio.charset.Charset;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.Security;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;
import java.util.stream.StreamSupport;

@Controller
public class NotesController {
    HttpSessionBean httpSessionBean;

    @Autowired
    private NoteRepository noteRepository;

    @Autowired
    public NotesController(HttpSessionBean httpSessionBean) throws NoSuchPaddingException, NoSuchAlgorithmException {this.httpSessionBean = httpSessionBean;}


    @Autowired
    private UserService userService;

    private static final String[] hashNames = {"Stribog256"};



        public static String streamAppend(Iterable<Note> chars){
        return StreamSupport.stream(chars.spliterator(), true)
                .collect(
                        StringBuilder::new,
                        StringBuilder::append,
                        StringBuilder::append
                )
                .toString();
    }

    private static byte [] simpleSymmetricEncrypt(
            Cipher cipher,
            SecretKey key,
            byte[] inputMas) throws InvalidKeyException, IllegalBlockSizeException, BadPaddingException {

        cipher.init(Cipher.ENCRYPT_MODE, key);

        return cipher.doFinal(inputMas);
    }

    private static byte [] simpleSymmetricDecrypt(
            Cipher cipher,
            SecretKey key,
            byte[] encryptMas) throws InvalidKeyException, IllegalBlockSizeException, BadPaddingException {

        cipher.init(Cipher.DECRYPT_MODE, key);

        return cipher.doFinal(encryptMas);
    }
    @GetMapping("/")
    public String greeting(Map<String, Object> model) {
        return "greeting";
    }

    @GetMapping("/notes")
    public String main(@RequestParam(required = false, defaultValue = "") String filter, Model model) throws NoSuchPaddingException, NoSuchAlgorithmException, IllegalBlockSizeException, BadPaddingException, InvalidKeyException {

        Security.addProvider(new BouncyCastleProvider());

        if (Security.getProvider("JStribog") == null) {
            Security.addProvider(new StribogProvider());
        }

        if (httpSessionBean.getMasterPass() == null) {
            return "redirect:/master-password";
        }

        Iterable<Note> notes = noteRepository.findAll();
        List<Note> dec = new ArrayList<>();

        Cipher cipher = Cipher.getInstance("GOST3412-2015");

        StringBuilder key = new StringBuilder();
        SecretKey myKey = null;

        for (String hashName : hashNames) {
            MessageDigest md = MessageDigest.getInstance(hashName);
            byte[] digest = md.digest(httpSessionBean.getMasterPass().getBytes());

            for (byte b : digest) {

                int iv = (int) b & 0xFF;
                if (iv < 0x10) {
                    key.append('0');
                }
                key.append(Integer.toHexString(iv).toUpperCase());
            }

            myKey = new SecretKeySpec((Hex.decode(String.valueOf(key))), 0, Hex.decode(String.valueOf(key)).length, "GOST3412-2015");
        }


        if (filter != null && !filter.isEmpty()) {
            notes = noteRepository.findByTitle(new String(Hex.encode(simpleSymmetricEncrypt(cipher, myKey, filter.getBytes()))));
        }

        for(Note note: notes) {
            String decTitle = new String(simpleSymmetricDecrypt(cipher, myKey, Hex.decode(note.getTitle().getBytes())));
            note.setTitle(decTitle);

            String decText = new String(simpleSymmetricDecrypt(cipher, myKey, Hex.decode(note.getText().getBytes())));
            note.setText(decText);

            dec.add(note);
        }

        model.addAttribute("notes", dec);
        model.addAttribute("filter", filter);

        return "main";
    }

    @PostMapping("/notes")
    public String add(
            @AuthenticationPrincipal User user,
            @Valid Note note,
            BindingResult bindingResult,
            Model model
    ) throws NoSuchPaddingException, NoSuchAlgorithmException, IllegalBlockSizeException, BadPaddingException, InvalidKeyException {
        Security.addProvider(new BouncyCastleProvider());

        if (Security.getProvider("JStribog") == null) {
            Security.addProvider(new StribogProvider());
        }

        Cipher cipher = Cipher.getInstance("GOST3412-2015");

        StringBuilder key = new StringBuilder();
        SecretKey myKey = null;

        for (String hashName : hashNames) {
            MessageDigest md = MessageDigest.getInstance(hashName);
            byte[] digest = md.digest(httpSessionBean.getMasterPass().getBytes());

            for (byte b : digest) {

                int iv = (int) b & 0xFF;
                if (iv < 0x10) {
                    key.append('0');
                }
                key.append(Integer.toHexString(iv).toUpperCase());
            }

            myKey = new SecretKeySpec((Hex.decode(String.valueOf(key))), 0, Hex.decode(String.valueOf(key)).length, "GOST3412-2015");
        }


        note.setAuthor(user);

        if (bindingResult.hasErrors()) {
            Map<String, String> errorsMap = ControllerUtils.getErrors(bindingResult);

            model.mergeAttributes(errorsMap);
            model.addAttribute("note", note);
        } else {



            note.setTitle(new String(Hex.encode(simpleSymmetricEncrypt(cipher, myKey, note.getTitle().getBytes()))));
            note.setText(new String(Hex.encode(simpleSymmetricEncrypt(cipher, myKey, note.getText().getBytes()))));

            noteRepository.save(note);
        }

        Iterable<Note> notes = noteRepository.findAll();

        if (note.getTitle().equals("")) {
            model.addAttribute("titleError", "Поле 'Заголовок' не должно быть пустым");
        }

        model.addAttribute("notes", notes);

        return "redirect:/notes";
    }


        @GetMapping("/note-edit/{user}")
    public String userNotes(
            @AuthenticationPrincipal User currentUser,
            @PathVariable User user,
            Model model,
            @RequestParam (required = false) Note note
    ) throws NoSuchPaddingException, NoSuchAlgorithmException, IllegalBlockSizeException, BadPaddingException, InvalidKeyException, ShortBufferException {

        Security.addProvider(new BouncyCastleProvider());
        Set<Note> notes = user.getNotes();

        if (httpSessionBean.getMasterPass() == null) {
            return "redirect:/master-password";
        }

        Cipher cipher = Cipher.getInstance("GOST3412-2015");

        StringBuilder key = new StringBuilder();
        SecretKey myKey = null;

        for (String hashName : hashNames) {
            MessageDigest md = MessageDigest.getInstance(hashName);
            byte[] digest = md.digest(httpSessionBean.getMasterPass().getBytes());

            for (byte b : digest) {

                int iv = (int) b & 0xFF;
                if (iv < 0x10) {
                    key.append('0');
                }
                key.append(Integer.toHexString(iv).toUpperCase());
            }

            myKey = new SecretKeySpec((Hex.decode(String.valueOf(key))), 0, Hex.decode(String.valueOf(key)).length, "GOST3412-2015");
        }

        String decTitle = new String(simpleSymmetricDecrypt(cipher, myKey, Hex.decode(note.getTitle().getBytes())));
        String decText = new String(simpleSymmetricDecrypt(cipher, myKey, Hex.decode(note.getText().getBytes())));

        note.setTitle(decTitle);
        note.setText(decText);

        model.addAttribute("notes", notes);
        model.addAttribute("note", note);
        model.addAttribute("isCurrentUser", currentUser.equals(user));

        return "userNotes";
    }

    @PostMapping("/note-edit/{user}")
    public String updateNote(
            @AuthenticationPrincipal User currentUser,
            @PathVariable Long user,
            @RequestParam("title") String title,
            @RequestParam("text") String text,
            @RequestParam("id") Note note
    ) throws NoSuchPaddingException, NoSuchAlgorithmException, IllegalBlockSizeException, BadPaddingException, InvalidKeyException {
        Security.addProvider(new BouncyCastleProvider());

        if (httpSessionBean.getMasterPass() == null) {
            return "redirect:/master-password";
        }



        Cipher cipher = Cipher.getInstance("GOST3412-2015");

        StringBuilder key = new StringBuilder();
        SecretKey myKey = null;

        for (String hashName : hashNames) {
            MessageDigest md = MessageDigest.getInstance(hashName);
            byte[] digest = md.digest(httpSessionBean.getMasterPass().getBytes());

            for (byte b : digest) {

                int iv = (int) b & 0xFF;
                if (iv < 0x10) {
                    key.append('0');
                }
                key.append(Integer.toHexString(iv).toUpperCase());
            }

            myKey = new SecretKeySpec((Hex.decode(String.valueOf(key))), 0, Hex.decode(String.valueOf(key)).length, "GOST3412-2015");
        }


        String encTitle = new String(Hex.encode(simpleSymmetricEncrypt(cipher, myKey, title.getBytes())));
        String encText = new String(Hex.encode(simpleSymmetricEncrypt(cipher, myKey, text.getBytes())));


        if (note.getAuthor().equals(currentUser)) {
            if (!StringUtils.isEmpty(title)) {
                note.setTitle(encTitle);
            }

            if (!StringUtils.isEmpty(text)) {
                note.setText(encText);
            }

            noteRepository.save(note);
        }
        return "redirect:/notes";
    }

    @PostMapping("/notes/{note}/delete")
    public String deleteNotes(@RequestParam("id") Note note, Model model) {

        noteRepository.delete(note);

        return "redirect:/notes";
    }

    @GetMapping("/master-password")
    public String mSC (String masterPass) {

        if (!StringUtils.isEmpty(masterPass)) {
            httpSessionBean.setMasterPass(masterPass);
        }

        return "master-password";
    }

    @PostMapping("/master-password")
    public String masterPassCheck(User user, Model model) throws NoSuchAlgorithmException, IOException {

        User userMP = (User) org.springframework.security.core.context.SecurityContextHolder
                .getContext().getAuthentication().getPrincipal();
        String currentMasterPassword = userMP.getMasterPassword();


        if (!userService.masterPasswordCheck(user)) {
            model.addAttribute("masterPasswordError", "Неверный мастер-пароль");
            return "master-password";
        }

        if (!StringUtils.isEmpty(user)) {
            httpSessionBean.setMasterPass(currentMasterPassword);
        }

        return "redirect:/notes";
    }

}
