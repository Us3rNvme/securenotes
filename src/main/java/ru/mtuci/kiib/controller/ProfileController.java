package ru.mtuci.kiib.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.mtuci.kiib.domain.User;
import ru.mtuci.kiib.repository.UserRepository;
import ru.mtuci.kiib.service.UserService;

import java.util.Map;

@Controller
public class ProfileController {

    @Autowired
    private UserService userService;

    @Autowired
    private UserRepository userRepository;

    @GetMapping("/user/profile")
    public String getProfile(Model model, @AuthenticationPrincipal User user) {
        model.addAttribute("username", user.getUsername());
        model.addAttribute("email", user.getEmail());

        return "profile";
    }

    @PostMapping("/user/profile")
    public String updateProfile(
            @AuthenticationPrincipal User user,
            @RequestParam String email,
            @RequestParam String password,
            Map<String, Object> model
    ) {

        if (!StringUtils.isEmpty(password)) {
            model.put("message", "Пароль успешно изменен");
        }

        if (!userService.updateProfile(user, email, password)) {
            if (userRepository.findByEmail(user.getEmail()) != null) {
                model.put("emailError", "Пользователь с такой электронной почтой уже существует");
            }

        } else  {
            model.put("message", "Для изменения электронной почты перейдите по ссылке из письма");
        }

        if (!StringUtils.isEmpty(password)) {
            model.put("message", "Пароль успешно изменен");
        }

        return "profile";
    }
}
