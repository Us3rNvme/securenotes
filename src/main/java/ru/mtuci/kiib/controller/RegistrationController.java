package ru.mtuci.kiib.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import ru.mtuci.kiib.domain.User;
import ru.mtuci.kiib.repository.UserRepository;
import ru.mtuci.kiib.service.UserService;

import javax.validation.Valid;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.util.Map;

@Controller
public class RegistrationController {
    @Autowired
    private UserService userService;

    @Autowired
    private UserRepository userRepository;

    @GetMapping("/registration")
    public String registration() {
        return "registration";
    }

    @PostMapping("/registration")
    public String addUser(@Valid User user, BindingResult bindingResult, Model model) throws UnsupportedEncodingException, NoSuchAlgorithmException, NoSuchProviderException {

        if (user.getUsername().equals("")) {
            model.addAttribute("usernameError", "Заполните это поле");
        }

        if (user.getPassword().equals("")) {
            model.addAttribute("passwordError", "Заполните это поле");
        }

        if (user.getMasterPassword().equals("")) {
            model.addAttribute("masterPasswordError", "Заполните это поле");
        }

        if (user.getEmail().equals("")) {
            model.addAttribute("emailError", "Заполните это поле");
        }

        if (bindingResult.hasErrors()) {
            Map<String, String> errors = ControllerUtils.getErrors(bindingResult);

            model.addAttribute(errors);

            return "registration";
        }

        if (!userService.addUser(user)) {
            if (user.getPassword().equals(user.getMasterPassword())) {
                model.addAttribute("masterPasswordError", "Поля 'Пароль' и 'Мастер-пароль' не должны совпадать");
            }
            if ((userRepository.findByUsername(user.getUsername()) != null) &&
                    (userRepository.findByEmail(user.getEmail()) != null)) {
                model.addAttribute("usernameError", "Пользователь с таким логином уже существует");
                model.addAttribute("emailError", "Пользователь с таким адресом электронной почты уже существует");
            }
            if (userRepository.findByUsername(user.getUsername()) != null) {
                model.addAttribute("usernameError", "Пользователь с таким логином уже существует");
            }
            if (userRepository.findByEmail(user.getEmail()) != null) {
                model.addAttribute("emailError", "Пользователь с таким адресом электронной почты уже существует");
            }

            return "registration";
        }

        return "redirect:/login";
    }

    @GetMapping("/activation/{code}")
    public String activate(Model model, @PathVariable String code) {
        boolean isActivated = userService.activateUser(code);

        if (isActivated) {
            model.addAttribute("message", "Пользователь успешно активирован");
        } else {
            model.addAttribute("message", "Код активации не найден");
        }

        return "profile";
    }
}
