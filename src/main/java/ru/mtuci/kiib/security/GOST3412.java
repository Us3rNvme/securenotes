package ru.mtuci.kiib.security;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.util.encoders.Hex;

import javax.crypto.Cipher;
import javax.crypto.*;
import javax.crypto.spec.SecretKeySpec;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.Security;

public class GOST3412 {
    public static SecretKey generateSymmetricKey(String algo) throws NoSuchAlgorithmException {
        KeyGenerator kg = KeyGenerator.getInstance(algo);
        return kg.generateKey();
    }

    public static byte [] simpleSymmetricEncrypt(
            Cipher cipher,
            SecretKey key,
            byte[] inputMas) throws InvalidKeyException, IllegalBlockSizeException, BadPaddingException {

        cipher.init(Cipher.ENCRYPT_MODE, key);

        return cipher.doFinal(inputMas);
    }

    public static byte [] simpleSymmetricDecrypt(
            Cipher cipher,
            SecretKey key,
            byte[] encryptMas) throws InvalidKeyException, IllegalBlockSizeException, BadPaddingException {

        cipher.init(Cipher.DECRYPT_MODE, key);

        return cipher.doFinal(encryptMas);
    }

    public static void main(String[] args) throws Exception {
        Security.addProvider(new BouncyCastleProvider());


        byte inputMessage[] = "Рандом текст".getBytes();

        String algo = "GOST3412-2015";

        Cipher cipher = Cipher.getInstance("GOST3412-2015");
        String string = "BB0DC0C09BEBFA2126240BABD3CB9119AC96AB3EACD36F56001B53FE1B3191CA";
        byte[] mas =  string.getBytes();
        System.out.println("ARRARARARARA " + new String(mas));


        SecretKey myKey = new SecretKeySpec(Hex.decode(mas), 0, Hex.decode(mas).length, "GOST3411");
        System.out.println("Секретный ключ:\t\t\t\t" + new String(Hex.encode(myKey.getEncoded())));


        byte encMas[] = simpleSymmetricEncrypt(cipher, myKey, inputMessage);

        byte decMas[] = simpleSymmetricDecrypt(cipher, myKey, Hex.decode("eb2049c0721413ccc97c63cf35800e7907ecd97f5ce1cd0a1138f1f782d31d20".getBytes()));

        System.out.println("Алгоритм:\t\t\t\t\t" + algo);

        System.out.println("Секретный ключ:\t\t\t\t" + new String(Hex.encode(myKey.getEncoded())));
        System.out.println("Зашифрованное сообщение:\t" + new String(Hex.encode(encMas)));
        System.out.println("Расшифрованное сообщение:\t" + new String(decMas));
        System.out.println("----------------------");
    }
}
