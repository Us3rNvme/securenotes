package ru.mtuci.kiib.security;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Security;
import java.util.Arrays;

public class UsageSample {

    private static final String[] hashNames = {"Stribog256"};
    private static final String[] reversedHashNames = {"StribogB256"}; //Use this digests for loooong messages from streams

    private static final byte[] message = "Qwerty012344321".getBytes();

    public static void main(String[] argv) throws NoSuchAlgorithmException, NoSuchProviderException {
        if (Security.getProvider("JStribog") == null) {
            Security.addProvider(new StribogProvider());
        }
        for (String hashName : hashNames) {
            MessageDigest md = MessageDigest.getInstance(hashName);
            byte[] digest = md.digest(message);
            printHex(digest);
        }
        for (String hashName : reversedHashNames) {
            MessageDigest md = MessageDigest.getInstance(hashName);
            byte[] digest = md.digest(reverse(message));
            printHex(digest);
        }

    }

    private static void printHex(byte[] digest) {
        for (byte b : digest) {
            int iv = (int) b & 0xFF;
            if (iv < 0x10) {
                System.out.print('0');
            }
            System.out.print(Integer.toHexString(iv).toUpperCase() + "");
        }
        System.out.println();
    }

    private static byte[] reverse(byte[] ba) {
        byte[] result = new byte[ba.length];
        for (int i = ba.length - 1; i >= 0; i--) {
            result[ba.length - 1 - i] = ba[i];
        }
        return result;
    }

}
