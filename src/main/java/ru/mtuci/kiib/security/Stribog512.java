package ru.mtuci.kiib.security;

import ru.mtuci.kiib.security.Stribog;

public final class Stribog512 extends Stribog {
    private static final int[] IV = new int[64];

    @Override
    protected byte[] engineDigest() {
        return getDigest(IV);
    }

}