package ru.mtuci.kiib.DB;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBNotes {
    private final String HOST = "jdbc:mysql://localhost:3306/secure_notes?useUnicode=true&serverTimezone=Europe/Moscow";
    private final String USERNAME = "root";
    private final String PASSWORD = "A1b2C3d4qwerty";

    private Connection connection;

    public DBNotes() {
        try {
            connection = DriverManager.getConnection(HOST, USERNAME, PASSWORD);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Connection getConnection() {
        return connection;
    }

    public void setConnection(Connection connection) {
        this.connection = connection;
    }
}
