package ru.mtuci.kiib.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import ru.mtuci.kiib.domain.Role;
import ru.mtuci.kiib.domain.User;
import ru.mtuci.kiib.repository.UserRepository;
import ru.mtuci.kiib.security.StribogProvider;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.Security;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class UserService implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private MailSender mailSender;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return userRepository.findByUsername(username);
    }

    String[] hashNames = {"Stribog512"};

    public boolean addUser(User user) throws NoSuchAlgorithmException {
        if (Security.getProvider("JStribog") == null) {
            Security.addProvider(new StribogProvider());
        }

        User userFromDb = userRepository.findByUsername(user.getUsername());
        User userFromDb2 = userRepository.findByEmail(user.getEmail());

        if (userFromDb != null || userFromDb2 != null) {
            return false;
        }

        user.setActive(true);
        user.setRoles(Collections.singleton(Role.USER));
        user.setActivationCode(UUID.randomUUID().toString());
        user.setPassword(user.getPassword());
        user.setMasterPassword(user.getMasterPassword());

        if (user.getPassword().equals(user.getMasterPassword())) {
            return false;
        }

        user.setPassword(passwordEncoder.encode(user.getPassword()));

        StringBuilder masterPass = new StringBuilder();

        for (String hashName : hashNames) {
            MessageDigest md = MessageDigest.getInstance(hashName);
            byte[] digest = md.digest(user.getMasterPassword().getBytes());

            for (byte b : digest) {

                int iv = (int) b & 0xFF;
                if (iv < 0x10) {
                    masterPass.append('0');
                }
                masterPass.append(Integer.toHexString(iv).toUpperCase());
            }

            user.setMasterPassword(masterPass.toString());
        }
        userRepository.save(user);

        sendMessage(user);

        return true;
    }

    private void sendMessage(User user) {
        if (!StringUtils.isEmpty(user.getEmail())) {
            String message = String.format(
                    "Hello, %s! \n" +
                            "Please, visit this link for confirm the action: http://localhost:8080/activation/%s",
                    user.getUsername(),
                    user.getActivationCode()
            );

            mailSender.send(user.getEmail(), "Activation code", message);
        }
    }

    public boolean activateUser(String code) {
        User user = userRepository.findByActivationCode(code);
        if (user == null) {
            return false;
        }
        user.setActivationCode(null);

        userRepository.save(user);

        return true;
    }

    public List<User> findAll() {
        return userRepository.findAll();
    }

    public void saveUser(User user, String username, Map<String, String> form) {
        user.setUsername(username);

        Set<String> roles = Arrays.stream(Role.values())
                .map(Role::name)
                .collect(Collectors.toSet());

        user.getRoles().clear();

        for (String key : form.keySet()) {
            if (roles.contains(key)) {
                user.getRoles().add(Role.valueOf(key));
            }
        }

        userRepository.save(user);
    }

    public boolean updateProfile(User user, String email, String password) {
        String userEmail = user.getEmail();

        boolean isEmailChanged = email != null || userEmail != null;

        if (!StringUtils.isEmpty(password)) {
            user.setPassword(passwordEncoder.encode(password));
            return true;
        }

        if ((isEmailChanged && userRepository.findByEmail(email) == null) || Objects.equals(email, userEmail))  {
            user.setEmail(email);

            if (!StringUtils.isEmpty(email)) {
                user.setActivationCode(UUID.randomUUID().toString());
            }

            userRepository.save(user);

            sendMessage(user);

            return true;
        }
        else {
            return false;
        }
    }

    public boolean masterPasswordCheck(User user) throws NoSuchAlgorithmException {

        StringBuilder masterPassword = new StringBuilder();

        User userMP = (User) org.springframework.security.core.context.SecurityContextHolder
                .getContext().getAuthentication().getPrincipal();
        String currentMasterPassword = userMP.getMasterPassword();


        if (Security.getProvider("JStribog") == null) {
            Security.addProvider(new StribogProvider());
        }

        for (String hashName : hashNames) {
            MessageDigest md = MessageDigest.getInstance(hashName);
            byte[] digest = md.digest(user.getMasterPassword().getBytes());

            for (byte b : digest) {
                int iv = (int) b & 0xFF;
                if (iv < 0x10) {
                    masterPassword.append('0');
                }
                masterPassword.append(Integer.toHexString(iv).toUpperCase());
            }

        }


        if (!String.valueOf(masterPassword).equals(currentMasterPassword)) {
            return false;
        }

        return true;
    }
}

