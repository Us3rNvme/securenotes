package ru.mtuci.kiib.repository;

import org.springframework.data.repository.CrudRepository;
import ru.mtuci.kiib.domain.Note;

import java.util.List;

public interface NoteRepository extends CrudRepository<Note, Long> {
    List<Note> findByTitle(String title);

    List<Note> findByTitleAndText(String title, String text);

}
