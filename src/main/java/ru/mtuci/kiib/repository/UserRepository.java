package ru.mtuci.kiib.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.mtuci.kiib.domain.User;

public interface UserRepository extends JpaRepository<User, Long>{
    User findByUsername(String username);

    User findByEmail (String email);

    User findByMasterPassword (String masterPassword);

    User findByActivationCode(String code);
}
